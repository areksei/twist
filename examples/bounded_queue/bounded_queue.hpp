#pragma once

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/condition_variable.hpp>

#include <queue>

template <typename T>
class BoundedQueue {
 public:
  BoundedQueue(size_t capacity, bool notify_all = false)
      : capacity_(capacity), notify_all_(notify_all) {
  }

  void Put(T value) {
    std::unique_lock lock(mutex_);
    while (IsFull()) {
      changed_.wait(lock);
    }
    items_.push(std::move(value));
    NotifyChange();
  }

  T Get() {
    std::unique_lock lock(mutex_);
    while (IsEmpty()) {
      changed_.wait(lock);
    }
    T value = std::move(items_.front());
    items_.pop();
    NotifyChange();
    return value;
  }

 private:
  bool IsFull() const {
    return items_.size() == capacity_;
  }

  bool IsEmpty() const {
    return items_.empty();
  }

  void NotifyChange() {
    if (notify_all_) {
      changed_.notify_all();
    } else {
      // =(
      changed_.notify_one();
    }
  }

 private:
  const size_t capacity_;
  const bool notify_all_;

  std::queue<T> items_;
  twist::stdlike::mutex mutex_;
  // Bad design
  // Use two separate condvars for producers / consumers
  twist::stdlike::condition_variable changed_;
};
