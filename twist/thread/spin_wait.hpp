#pragma once

#include <wheels/support/cpu.hpp>

#include <thread>

namespace twist::thread {

// Usage:
//
// while (locked.exchange(true)) {
//   spin_wait();
// }
//
// If possible, use `IsEnough` method to check
// when it is advised to stop spinning and block the current thread
//
// Implementation is borrowed from
// https://github.com/crossbeam-rs/crossbeam/blob/master/crossbeam-utils/src/backoff.rs

class [[nodiscard]] SpinWait {
  static const size_t kSpinLimit = 6;
  static const size_t kYieldLimit = 10;

 public:
  void Spin() {
    if (iter_ <= kSpinLimit) {
      // Exponential backoff
      for (size_t i = 0; i < (1 << iter_); ++i) {
        wheels::SpinLockPause();
      }
    } else {
      std::this_thread::yield();
    }
    ++iter_;
  }

  void operator()() {
    Spin();
  }

  // Advice to stop spinning and block the current thread
  bool IsEnough() const {
    return iter_ > kYieldLimit;
  }

 private:
  size_t iter_{0};
};

}  // namespace twist::thread
