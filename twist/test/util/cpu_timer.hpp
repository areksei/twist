#pragma once

#include <wheels/support/cpu_time.hpp>

namespace twist::test::util {

using wheels::ProcessCPUTimer;
using wheels::ThreadCPUTimer;

using CPUTimer = ProcessCPUTimer;

}  // namespace twist::test::util
