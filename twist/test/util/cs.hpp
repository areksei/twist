#pragma once

#include <twist/test/util/plate.hpp>

namespace twist::test::util {

class CriticalSection {
 public:
  void operator()() {
    shared_plate_.Access();
  }

 private:
  Plate shared_plate_;
};

}  // namespace twist::test::util
