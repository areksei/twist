#pragma once

#include <twist/test/test.hpp>

#include <wheels/test/test_framework.hpp>
#include <wheels/support/preprocessor.hpp>

#include <chrono>
#include <tuple>

namespace twist::test {

namespace detail {

template <typename... Args>
class TestRunsRegistrar {
  using Self = TestRunsRegistrar;

  using TestRoutine = std::function<void(Args... args)>;
  using TL = std::chrono::nanoseconds;

 public:
  TestRunsRegistrar(std::string suite, void (*routine)(Args... args))
      : suite_(suite), routine_(routine) {
  }

  Self* operator->() {
    return this;
  }

  Self& Run(Args... args) {
    auto args_tuple = std::make_tuple(args...);

    auto run = [routine = routine_, args_tuple]() {
      std::apply(routine, args_tuple);
    };

    auto twist_run = [run]() {
      twist::test::RunTestRoutine(run);
    };

    Add(twist_run);

    return *this;
  }

  Self& Iterate(Args... args) {
    auto args_tuple = std::make_tuple(args...);

    auto iter_run = [routine = routine_, args_tuple]() {
      std::apply(routine, args_tuple);
    };

    auto twist_iterate = [iter_run]() {
      twist::test::RunIteratedTestRoutine(iter_run);
    };

    Add(twist_iterate);

    return *this;
  }

  Self& TimeLimit(TL value) {
    time_limit_ = value;
    return *this;
  }

 private:
  void Add(std::function<void()> test_run) {
    std::string name = MakeRunName(++index_);
    auto options = wheels::test::TestOptions().TimeLimit(time_limit_);

    auto test = wheels::test::MakeTest(test_run, name, suite_, options);
    wheels::test::RegisterTest(std::move(test));
  }

  static std::string MakeRunName(size_t index) {
    return std::string("Run-") + std::to_string(index);
  }

 private:
  std::string suite_;
  TestRoutine routine_;
  TL time_limit_{10s};
  size_t index_{0};
};

}  // namespace detail

}  // namespace twist::test

#define TWIST_TEST_RUNS(suite, test_routine)                 \
  static twist::test::detail::TestRunsRegistrar UNIQUE_NAME( \
      twist_runs_registrar__) =                              \
      twist::test::detail::TestRunsRegistrar(#suite, test_routine)
