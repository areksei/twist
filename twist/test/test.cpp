#include <twist/test/test.hpp>

#if defined(TWIST_FAULTY)
#include <twist/fault/adversary/adversary.hpp>
#endif

#if defined(TWIST_FIBERS)

#include <twist/fiber/runtime/scheduler.hpp>
#include <wheels/test/test_framework.hpp>

// std::hash
#include <functional>

namespace twist::test {

static size_t GetTestRandomSeed() {
  return wheels::test::TestHash();
}

void RunTestRoutineInsideScheduler(TestRoutine test_routine) {
#if defined(TWIST_FAULTY)
  fault::AccessAdversary();
#endif

  test_routine();

#if defined(TWIST_FAULTY)
  fault::GetAdversary()->PrintReport();
#endif
}

void RunTestRoutine(TestRoutine test_routine) {
  size_t seed = GetTestRandomSeed();
  std::cout << "Seed for deterministic execution: " << seed << std::endl;

  twist::fiber::Scheduler scheduler{seed};
  scheduler.Run([test_routine]() {
    RunTestRoutineInsideScheduler(test_routine);
  });
}

static size_t GetIteratedTestRandomSeed() {
  return wheels::test::TestIterationHash();
}

void RunIteratedTestRoutine(TestRoutine test_routine) {
  size_t seed = GetIteratedTestRandomSeed();
  // std::cout << "Seed for deterministic execution: " << seed << std::endl;

  twist::fiber::Scheduler scheduler{seed};
  scheduler.Run(std::move(test_routine));
}

}  // namespace twist::test

#else  // Threads

namespace twist::test {

void RunTestRoutine(TestRoutine test_routine) {
  test_routine();

#if defined(TWIST_FAULTY)
  fault::GetAdversary()->PrintReport();
#endif
}

void RunIteratedTestRoutine(TestRoutine test_routine) {
  test_routine();
}

}  // namespace twist::test

#endif

namespace twist::test {

bool KeepRunning() {
  return wheels::test::TestTimeLeft() > 500ms;
}

}  // namespace twist::test