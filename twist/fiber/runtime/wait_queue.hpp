#pragma once

#include <twist/fiber/runtime/fwd.hpp>

#include <wheels/intrusive/list.hpp>

#include <string>

namespace twist {
namespace fiber {

class WaitQueue {
 public:
  WaitQueue(const std::string& descr = "?");

  void Park();

  void WakeOne();
  void WakeAll();

 private:
  const std::string descr_;
  wheels::IntrusiveList<Fiber> waiters_;
};

}  // namespace fiber
}  // namespace twist
