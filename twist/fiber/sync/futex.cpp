#include <twist/fiber/sync/futex.hpp>

#include <twist/fiber/runtime/wait_queue.hpp>

#include <wheels/support/singleton.hpp>

#include <map>

namespace twist {
namespace fiber {

class WaitQueues {
 public:
  WaitQueue& Access(void* address) {
    auto [it, _] = queues_.try_emplace(address, "futex");
    return it->second;
  }

 private:
  std::map<void*, WaitQueue> queues_;
};

WaitQueue& WaitQueueFor(void* address) {
  return Singleton<WaitQueues>()->Access(address);
}

}  // namespace fiber
}  // namespace twist
