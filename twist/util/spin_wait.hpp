#pragma once

#include <twist/single_core.hpp>

#if TWIST_SINGLE_CORE

#include <twist/strand/stdlike/thread.hpp>

namespace twist::util {

struct [[nodiscard]] SpinWait {
  void Spin() {
    Yield();
  }

  void operator()() {
    Spin();
  }

  bool IsEnough() const {
    return true;
  }

 private:
  void Yield() {
    strand::stdlike::this_thread::yield();
  }
};

}  // namespace twist::util

#else

#include <twist/thread/spin_wait.hpp>

namespace twist::util {

using thread::SpinWait;

}  // namespace twist::util

#endif
