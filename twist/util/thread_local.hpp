#pragma once

#include <twist/strand/thread_local.hpp>

namespace twist::util {

using strand::ThreadLocal;
using strand::ThreadLocalPtr;

}  // namespace twist::util
