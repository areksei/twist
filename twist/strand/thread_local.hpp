#pragma once

#include <twist/strand/tls.hpp>

#include <wheels/support/noncopyable.hpp>

namespace twist {
namespace strand {

//////////////////////////////////////////////////////////////////////

/*
 * Usage:
 *
 * ThreadLocal<int> value{256};
 * Store: *value = 1024;
 * Load: int this_thread_value = *value;
 */

template <typename T>
class ThreadLocal : private wheels::NonCopyable {
 public:
  ThreadLocal(TLSManager::Ctor ctor, TLSManager::Dtor dtor) {
    Init(ctor, dtor);
  }

  explicit ThreadLocal(T default_value) {
    InitWithConstructor([default_value]() -> void* {
      return new T(default_value);
    });
  }

  explicit ThreadLocal() {
    InitWithConstructor([]() -> void* {
      return new T;
    });
  }

  T& Get() {
    return *GetPointer();
  }

  // Access thread-local instance
  T& operator*() {
    return Get();
  }

  // Usage: thread_local_obj->Method();
  T* operator->() {
    return GetPointer();
  }

 private:
  void Init(TLSManager::Ctor ctor, TLSManager::Dtor dtor) {
    slot_index_ = TLSManager::Instance().AcquireSlot(ctor, dtor);
  }

  void InitWithConstructor(TLSManager::Ctor ctor) {
    auto dtor = [](void* ptr) {
      delete (T*)ptr;
    };

    Init(ctor, dtor);
  }

  T* GetPointer() {
    T* data = (T*)TLSManager::Instance().Access(slot_index_);
    WHEELS_VERIFY(data != nullptr, "Not initialized");
    return data;
  }

 private:
  size_t slot_index_;
};

//////////////////////////////////////////////////////////////////////

/*
 * Usage:
 *
 * ThreadLocalPtr<int> ptr;  // nullptr
 * Store: ptr = new int;
 * Load: int value = *ptr;
 */

template <typename T>
class ThreadLocalPtr : private wheels::NonCopyable {
 public:
  ThreadLocalPtr() {
    Init();
  }

  operator T*() {
    return LoadTypedPointer();
  }

  ThreadLocalPtr& operator=(T* ptr) {
    StorePointer(ptr);
    return *this;
  }

  T* Exchange(T* ptr) {
    T* old_ptr = LoadTypedPointer();
    StorePointer(ptr);
    return old_ptr;
  }

  // Usage: thread_local_ptr->Method();
  T* operator->() {
    return LoadTypedPointer();
  }

  explicit operator bool() const {
    return LoadTypedPointer() != nullptr;
  }

 private:
  void Init() {
    auto ctor = []() {
      return nullptr;
    };
    auto dtor = [](void* /*raw_ptr*/) {
      // Nop
    };
    slot_index_ = TLSManager::Instance().AcquireSlot(ctor, dtor);
  }

  T* LoadTypedPointer() {
    void** slot = AccessSlot();
    void* raw_ptr = *slot;
    return (T*)raw_ptr;
  }

  void StorePointer(T* ptr) {
    void** slot = AccessSlot();
    *slot = ptr;
  }

  void** AccessSlot() {
    void*& slot = TLSManager::Instance().Access(slot_index_);
    return &slot;
  }

 private:
  size_t slot_index_;
};

}  // namespace strand
}  // namespace twist
