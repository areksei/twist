#pragma once

#if defined(TWIST_FIBERS)

// cooperative user-space fibers

#include <twist/fiber/stdlike/condvar.hpp>

namespace twist::strand::stdlike {

using condition_variable = fiber::CondVar;  // NOLINT

}  // namespace twist::strand::stdlike

#else

// native threads

#include <condition_variable>

namespace twist::strand::stdlike {

using condition_variable = ::std::condition_variable;  // NOLINT

}  // namespace twist::strand::stdlike

#endif
