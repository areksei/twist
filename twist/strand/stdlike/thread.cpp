#include <twist/strand/stdlike.hpp>

namespace twist::strand::stdlike {

#if defined(TWIST_FIBERS)

const thread_id kInvalidThreadId = -1;

#else

const std::thread::id kInvalidThreadId{};

#endif

}  // namespace twist::strand::stdlike
