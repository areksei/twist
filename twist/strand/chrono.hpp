#pragma once

#if defined(TWIST_FIBERS)

#include <twist/fiber/runtime/chrono.hpp>

namespace twist::strand {

using SteadyClock = fiber::FiberSteadyClock;

}  // namespace twist::strand

#else

#include <chrono>

namespace twist::strand {

using SteadyClock = std::chrono::steady_clock;

}  // namespace twist::strand

#endif