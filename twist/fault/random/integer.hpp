#pragma once

#include <twist/fault/random/source.hpp>

namespace twist::fault {

// [0, max]
inline uint64_t RandomUInteger(uint64_t max) {
  return RandomUInt64() % (max + 1);
}

// [lo, hi]
inline uint64_t RandomUInteger(uint64_t lo, uint64_t hi) {
  return lo + RandomUInteger(hi - lo);
}

}  // namespace twist::fault
