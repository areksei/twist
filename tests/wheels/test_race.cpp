#include <twist/test/util/race.hpp>

#include <wheels/test/test_framework.hpp>

#include <thread>

using namespace std::chrono_literals;

TEST_SUITE(Race) {

SIMPLE_TEST(JustWorks) {
  static const size_t kThreads = 4;

  twist::test::util::Race race{kThreads};

  size_t finished = 0;

  for (size_t i = 0; i < kThreads; ++i) {
    race.Add([&]() {
      std::this_thread::sleep_for(500ms);
      ++finished;
    });
  }

  std::this_thread::sleep_for(1s);
  ASSERT_EQ(finished, 0);

  race.Run();

  ASSERT_EQ(finished, kThreads);
}

}
