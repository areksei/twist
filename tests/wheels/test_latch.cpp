#include <twist/test/util/latch.hpp>

#include <wheels/test/test_framework.hpp>

#include <thread>
#include <chrono>

using namespace std::chrono_literals;

TEST_SUITE(CountDownLatch) {
  SIMPLE_TEST(Empty) {
    twist::test::util::CountDownLatch latch{0};
    latch.Await();  // doesn't wait
  }

  SIMPLE_TEST(CountDownOne) {
    twist::test::util::CountDownLatch latch{1};
    latch.CountDown();
    latch.Await();  // doesn't wait
  }

  SIMPLE_TEST(WaitForCountDown) {
    twist::test::util::CountDownLatch latch{2};

    auto countdown_routine = [&latch]() {
      std::this_thread::sleep_for(500ms);
      latch.CountDown();
      std::this_thread::sleep_for(500ms);
      latch.CountDown();
    };

    std::thread countdown_thread(countdown_routine);

    latch.Await(); // ~1 second

    countdown_thread.join();
  }
}
